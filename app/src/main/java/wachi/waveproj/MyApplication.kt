package wachi.waveproj

import android.app.Application
import wachi.waveproj.common.dependencyinjection.app.AppComponent
import wachi.waveproj.common.dependencyinjection.app.AppModule
import wachi.waveproj.common.dependencyinjection.app.DaggerAppComponent

class MyApplication: Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
    }
}