package wachi.waveproj.common.dependencyinjection.service

import dagger.Component
import dagger.Subcomponent

@Subcomponent(modules = [ServiceModule::class])
interface ServiceComponent {
}