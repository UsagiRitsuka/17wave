package wachi.waveproj.common.dependencyinjection.activity

import javax.inject.Scope

@Scope
annotation class ActivityScope {
}