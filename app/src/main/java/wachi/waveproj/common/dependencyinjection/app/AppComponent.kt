package wachi.waveproj.common.dependencyinjection.app

import dagger.Component
import wachi.waveproj.common.dependencyinjection.activity.ActivityComponent
import wachi.waveproj.common.dependencyinjection.service.ServiceComponent
import wachi.waveproj.common.dependencyinjection.service.ServiceModule

@AppScope
@Component(modules = [AppModule::class])
interface AppComponent{
    fun newActivityComponentBuilder(): ActivityComponent.Builder
    fun newServiceComponent(serviceModule: ServiceModule): ServiceComponent
}