package wachi.waveproj.common.dependencyinjection.app

import javax.inject.Scope

@Scope
annotation class AppScope {
}