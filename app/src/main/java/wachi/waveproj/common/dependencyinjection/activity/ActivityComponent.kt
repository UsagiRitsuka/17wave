package wachi.waveproj.common.dependencyinjection.activity

import androidx.appcompat.app.AppCompatActivity
import dagger.BindsInstance
import dagger.Component
import dagger.Subcomponent
import wachi.waveproj.common.dependencyinjection.presentation.PresentationComponent
import wachi.waveproj.common.dependencyinjection.presentation.PresentationModule

@ActivityScope
@Subcomponent(modules = [ActivityModule::class])
interface ActivityComponent {
    fun newPresentationComponent(presentationModule: PresentationModule): PresentationComponent

    @Subcomponent.Builder
    interface Builder{
        @BindsInstance fun activity(activity: AppCompatActivity): Builder
        fun builder(): ActivityComponent
    }
}