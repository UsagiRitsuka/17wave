package wachi.waveproj.common.dependencyinjection.presentation

import dagger.Subcomponent
import wachi.waveproj.screens.search.SearchActivity
import wachi.waveproj.screens.search.SearchFragment
import wachi.waveproj.screens.userslist.UsersListActivity


@PresentationScope
@Subcomponent(modules = [PresentationModule::class, ViewModelModule::class])
interface PresentationComponent {
    fun inject(searchActivity: SearchActivity)
    fun inject(searchFragment: SearchFragment)
    fun inject(usersListActivity: UsersListActivity)
}