package wachi.waveproj.common.dependencyinjection.presentation

import androidx.savedstate.SavedStateRegistryOwner
import dagger.Module
import dagger.Provides

@Module
class PresentationModule(private val savedStateRegistry: SavedStateRegistryOwner) {
    @Provides
    fun savedStateRegistry() = savedStateRegistry
}