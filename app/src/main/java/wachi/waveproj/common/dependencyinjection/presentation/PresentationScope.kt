package wachi.waveproj.common.dependencyinjection.presentation

import javax.inject.Scope

@Scope
annotation class PresentationScope {
}