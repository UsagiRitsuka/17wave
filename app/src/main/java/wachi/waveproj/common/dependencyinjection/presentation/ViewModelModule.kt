package wachi.waveproj.common.dependencyinjection.presentation

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import wachi.waveproj.screens.userslist.UsersListViewModel

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(UsersListViewModel::class)
    abstract fun searchUsersListViewModel(searchUsersListViewModel: UsersListViewModel): ViewModel

}