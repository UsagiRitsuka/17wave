package wachi.waveproj

object Constants {
    const val SEARCH_STATE = "SEARCH_STATE"
    const val BASE_URL = "https://api.github.com/"
    const val PER_COUNT = 50
}