package wachi.waveproj.screens.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.coroutines.cancelChildren
import wachi.waveproj.screens.common.ScreensNavigator
import wachi.waveproj.screens.common.fragments.BaseFragment
import wachi.waveproj.screens.common.viewsmvx.ViewMvxFactory
import javax.inject.Inject

class SearchFragment: BaseFragment(), SearchViewMvx.Listener {

    @Inject lateinit var screensNavigator: ScreensNavigator
    @Inject lateinit var viewMvcFactory: ViewMvxFactory

    private lateinit var viewMvx: SearchViewMvx

    override fun onCreate(savedInstanceState: Bundle?) {
        injector.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewMvx = viewMvcFactory.newSearchViewMvx(container)
        return viewMvx.rootView
    }

    override fun onStart() {
        super.onStart()
        viewMvx.registerListener(this)
    }

    override fun onStop() {
        super.onStop()
        viewMvx.unregisterListener(this)
    }

    override fun onSearchClicked(keyWord: String) {
        screensNavigator.toUsersListActivity(keyWord)
    }
}