package wachi.waveproj.screens.search

import android.os.Bundle
import wachi.waveproj.R
import wachi.waveproj.screens.common.activity.BaseActivity

class SearchActivity: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_frame)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.frame_content, SearchFragment())
                .commit()
        }

    }
}