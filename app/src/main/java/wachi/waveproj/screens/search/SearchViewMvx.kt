package wachi.waveproj.screens.search

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import wachi.waveproj.R
import wachi.waveproj.screens.common.toolbar.MyToolbar
import wachi.waveproj.screens.common.viewsmvx.BaseViewMvx

class SearchViewMvx(
        inflater: LayoutInflater,
        parent: ViewGroup?
): BaseViewMvx<SearchViewMvx.Listener>(
        inflater,
        parent,
        R.layout.layout_search
) {

    interface Listener{
        fun onSearchClicked(keyWord: String)
    }

    private val myToolbar: MyToolbar
    private val searchBtn: Button
    private val inputEditText: EditText

    init {
        myToolbar = findViewById(R.id.toolbar)
        searchBtn = findViewById(R.id.btn_search)
        inputEditText = findViewById(R.id.input)

        searchBtn.setOnClickListener {
            if(inputEditText.text.toString().isNotEmpty()){
                listeners.forEach{listener ->
                    listener.onSearchClicked(inputEditText.text.toString())
                }
            }
        }
    }

}