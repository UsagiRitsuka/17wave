package wachi.waveproj.screens.common.viewsmvx

interface ObservableViewMvx<LISTENER_TYPE>: ViewMvx {
    fun registerListener(listener: LISTENER_TYPE)
    fun unregisterListener(listener: LISTENER_TYPE)
}