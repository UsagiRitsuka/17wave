package wachi.waveproj.screens.common.fragments

import androidx.fragment.app.Fragment
import wachi.waveproj.common.dependencyinjection.presentation.PresentationModule
import wachi.waveproj.screens.common.activity.BaseActivity

open class BaseFragment: Fragment() {
    private val presentationComponent by lazy {
        (requireActivity() as BaseActivity).activityComponent.newPresentationComponent(
                PresentationModule(this)
        )
    }

    protected val injector get() = presentationComponent
}