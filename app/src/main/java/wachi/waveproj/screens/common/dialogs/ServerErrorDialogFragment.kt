package wachi.waveproj.screens.common.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import wachi.waveproj.R

class ServerErrorDialogFragment: BaseDialog() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity).let {
            it.setTitle(R.string.server_error_title)
            it.setMessage(R.string.server_error_message)
            it.setPositiveButton(R.string.dialog_button) { _, _ -> dismiss() }
            it.create()
        }
    }

    companion object {
        fun newInstance(): ServerErrorDialogFragment {
            return ServerErrorDialogFragment()
        }
    }
}