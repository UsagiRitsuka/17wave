package wachi.waveproj.screens.common.imageloader

import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import wachi.waveproj.common.dependencyinjection.activity.ActivityScope
import javax.inject.Inject

@ActivityScope
class ImageLoader @Inject constructor(private val activity: AppCompatActivity){
    fun loadImage(imageUrl: String, target: ImageView) {
        Glide.with(activity).load(imageUrl).into(target)
    }
}