package wachi.waveproj.screens.common.activity

import androidx.appcompat.app.AppCompatActivity
import wachi.waveproj.MyApplication
import wachi.waveproj.common.dependencyinjection.presentation.PresentationModule

open class BaseActivity: AppCompatActivity() {

    private val appComponent get() = (application as MyApplication).appComponent
    val activityComponent by lazy {
        appComponent.newActivityComponentBuilder()
                .activity(this)
                .builder()
    }

    private val presentationComponent by lazy {
        activityComponent.newPresentationComponent(PresentationModule(this))
    }

    protected val injector get() = presentationComponent
}