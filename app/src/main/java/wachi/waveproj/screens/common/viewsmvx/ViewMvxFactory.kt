package wachi.waveproj.screens.common.viewsmvx

import android.view.LayoutInflater
import android.view.ViewGroup
import wachi.waveproj.screens.common.imageloader.ImageLoader
import wachi.waveproj.screens.search.SearchViewMvx
import wachi.waveproj.screens.userslist.UserItemViewMvx
import wachi.waveproj.screens.userslist.UsersListViewMvx
import wachi.waveproj.screens.userslist.UsersListViewMvxImpl
import javax.inject.Inject
import javax.inject.Provider

// just show 2 ways
// 可用介面隔離也可不用視狀況(不限此處)
class ViewMvxFactory @Inject constructor(
        private val inflaterProvider: Provider<LayoutInflater>,
        private val imageLoaderProvider: Provider<ImageLoader>,
        private val viewMvxFactoryProvider: Provider<ViewMvxFactory>
) {

    // return type: class
    fun newSearchViewMvx(parent: ViewGroup?): SearchViewMvx{
        return SearchViewMvx(inflaterProvider.get(), parent)
    }

    // return type: interface
    fun newUsersListViewMvx(parent: ViewGroup?): UsersListViewMvx {
        return UsersListViewMvxImpl(inflaterProvider.get(), parent, viewMvxFactoryProvider.get())
    }

    fun newUserItemViewMvx(parent: ViewGroup?): UserItemViewMvx {
        return UserItemViewMvx(inflaterProvider.get(), parent, imageLoaderProvider.get())
    }
}