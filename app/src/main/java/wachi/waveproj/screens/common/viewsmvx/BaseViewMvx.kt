package wachi.waveproj.screens.common.viewsmvx

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes

open class BaseViewMvx<LISTENER_TYPE>(
        private val inflater: LayoutInflater,
        private val parent: ViewGroup?,
        @LayoutRes private val layoutId: Int
): ObservableViewMvx<LISTENER_TYPE> {

    val rootView = inflater.inflate(layoutId, parent, false)
    protected val context get() = rootView.context
    protected val listeners = HashSet<LISTENER_TYPE>()

    override fun registerListener(listener: LISTENER_TYPE){
        listeners.add(listener)
    }

    override fun unregisterListener(listener: LISTENER_TYPE){
        listeners.remove(listener)
    }

    protected fun <T : View?> findViewById(@IdRes id: Int): T {
        return rootView.findViewById<T>(id)
    }

    override fun rootView(): View {
        return rootView
    }
}