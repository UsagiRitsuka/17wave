package wachi.waveproj.screens.common.viewsmvx

import android.view.View

interface ViewMvx {
    fun rootView(): View
}