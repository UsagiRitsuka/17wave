package wachi.waveproj.screens.common

import androidx.appcompat.app.AppCompatActivity
import wachi.waveproj.screens.userslist.UsersListActivity
import javax.inject.Inject

class ScreensNavigator @Inject constructor(private val activity: AppCompatActivity){
    fun navigateBack() {
        activity.onBackPressed()
    }

    fun toUsersListActivity(keyWord: String) {
        UsersListActivity.start(activity, keyWord)
    }
}