package wachi.waveproj.screens.common.dialogs

import androidx.fragment.app.DialogFragment
import wachi.waveproj.common.dependencyinjection.presentation.PresentationModule
import wachi.waveproj.screens.common.activity.BaseActivity

open class BaseDialog: DialogFragment() {
    private val presentationComponent by lazy {
        (requireActivity() as BaseActivity).activityComponent.newPresentationComponent(
                PresentationModule(this)
        )
    }

    protected val injector get() = presentationComponent
}