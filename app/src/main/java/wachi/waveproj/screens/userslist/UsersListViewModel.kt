package wachi.waveproj.screens.userslist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import kotlinx.coroutines.flow.Flow
import wachi.waveproj.Constants
import wachi.waveproj.screens.common.viewmodels.SavedStateViewModel
import wachi.waveproj.users.SearchUserPagingSource
import wachi.waveproj.users.User
import javax.inject.Inject

class UsersListViewModel @Inject constructor(): SavedStateViewModel(), SearchUserPagingSource.Listener{
    private lateinit var flow: Flow<PagingData<User>>

    private lateinit var _userPagingData: MutableLiveData<PagingData<User>>

    private lateinit var _isSearching: MutableLiveData<Boolean>
    val isSearching: LiveData<Boolean> get() = _isSearching

    override fun init(savedStateHandle: SavedStateHandle) {
        _isSearching = savedStateHandle.getLiveData(Constants.SEARCH_STATE)
    }

    fun searchUserFlow(searchUserPagingSource: SearchUserPagingSource): Flow<PagingData<User>>{
        if(!this::flow.isInitialized){
            searchUserPagingSource.searchStateListener = this
            flow = Pager(PagingConfig(pageSize = 20)) {
                searchUserPagingSource
            }.flow
            .cachedIn(viewModelScope)
        }

        return flow
    }

    override fun onSearchStart() {
        _isSearching.value = true
    }

    override fun onSearchFinish() {
        _isSearching.value = false
    }
}