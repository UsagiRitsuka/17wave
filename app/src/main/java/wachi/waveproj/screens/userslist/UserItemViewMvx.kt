package wachi.waveproj.screens.userslist

import android.view.LayoutInflater
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_user.view.*
import wachi.waveproj.R
import wachi.waveproj.screens.common.imageloader.ImageLoader
import wachi.waveproj.screens.common.viewsmvx.BaseViewMvx
import wachi.waveproj.users.User

class UserItemViewMvx(
        inflater: LayoutInflater,
        parent: ViewGroup?,
        private val imageLoader: ImageLoader
): BaseViewMvx<UserItemViewMvx.Listener>(
        inflater,
        parent,
        R.layout.item_user) {

    interface Listener{}


    fun bindUserInfo(user: User){
        rootView.name.text = user.name
        imageLoader.loadImage(user.avatarUrl, rootView.avatar)
    }
}