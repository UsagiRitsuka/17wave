package wachi.waveproj.screens.userslist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagingData
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import wachi.waveproj.R
import wachi.waveproj.screens.common.imageloader.ImageLoader
import wachi.waveproj.screens.common.toolbar.MyToolbar
import wachi.waveproj.screens.common.viewsmvx.BaseViewMvx
import wachi.waveproj.screens.common.viewsmvx.ViewMvxFactory
import wachi.waveproj.users.User

class UsersListViewMvxImpl(
    inflater: LayoutInflater,
    parent: ViewGroup?,
    viewMvxFactory: ViewMvxFactory
): UsersListViewMvx, BaseViewMvx<UsersListViewMvx.Listener>(
        inflater,
        parent,
        R.layout.layout_users_list
) {

    private val toolbar: MyToolbar
    private val swipe: SwipeRefreshLayout
    private val recyclerView: RecyclerView
    private val userAdapter: UserAdapter

    init {
        toolbar = findViewById(R.id.toolbar)
        swipe = findViewById(R.id.swip)
        recyclerView = findViewById(R.id.recycler_view)
        toolbar.setNavigateUpListener{
            listeners.forEach{listener ->
                listener.onBackClicked()
            }
        }

        swipe.isEnabled = false
        userAdapter = UserAdapter(UserComparator, viewMvxFactory)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = userAdapter
    }

    override fun showProgressIndicator(){
        swipe.isRefreshing = true
    }

    override fun hideProgressIndication() {
        swipe.isRefreshing = false
    }

    override suspend fun suspendBindPagingData(pagingData: PagingData<User>) {
        userAdapter.submitData(pagingData)
    }

    override fun rootView(): View {
        return rootView
    }

    class UserAdapter(
            diffCallback: DiffUtil.ItemCallback<User>,
            private val viewMvxFactory: ViewMvxFactory
    ): PagingDataAdapter<User, UserAdapter.UserViewHolder>(diffCallback){
        inner class UserViewHolder(val userItemViewMvx: UserItemViewMvx): RecyclerView.ViewHolder(userItemViewMvx.rootView)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
            return UserViewHolder(viewMvxFactory.newUserItemViewMvx(parent))
        }

        override fun onBindViewHolder(viewHolder: UserViewHolder, position: Int) {
            viewHolder.userItemViewMvx.bindUserInfo(getItem(position)!!)
        }
    }

    object UserComparator : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.name == newItem.name && oldItem.avatarUrl == newItem.avatarUrl
        }

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem == newItem
        }
    }
}