package wachi.waveproj.screens.userslist

import androidx.paging.PagingData
import wachi.waveproj.screens.common.viewsmvx.ObservableViewMvx
import wachi.waveproj.users.User

interface UsersListViewMvx: ObservableViewMvx<UsersListViewMvx.Listener> {
    interface Listener{
        fun onBackClicked()
    }

    fun showProgressIndicator()
    fun hideProgressIndication()
    suspend fun suspendBindPagingData(pagingData: PagingData<User>)
}