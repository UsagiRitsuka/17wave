package wachi.waveproj.screens.userslist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collectLatest
import wachi.waveproj.screens.common.ScreensNavigator
import wachi.waveproj.screens.common.activity.BaseActivity
import wachi.waveproj.screens.common.dialogs.DialogsNavigator
import wachi.waveproj.screens.common.viewmodels.ViewModelFactory
import wachi.waveproj.screens.common.viewsmvx.ViewMvxFactory
import wachi.waveproj.users.FetchResultOfUserSearchUseCase
import wachi.waveproj.users.SearchUserPagingSource
import javax.inject.Inject

class UsersListActivity: BaseActivity(), UsersListViewMvx.Listener {
    private val coroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.Main.immediate)
    @Inject lateinit var screensNavigator: ScreensNavigator
    @Inject lateinit var viewMvxFactory: ViewMvxFactory
    @Inject lateinit var fetchResultOfUserSearchUseCase: FetchResultOfUserSearchUseCase
    @Inject lateinit var dialogsNavigator: DialogsNavigator
    @Inject lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewMvx: UsersListViewMvx
    private lateinit var keyword: String

    private lateinit var viewModel: UsersListViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        injector.inject(this)
        super.onCreate(savedInstanceState)

        viewMvx = viewMvxFactory.newUsersListViewMvx(null)
        setContentView(viewMvx.rootView())

        keyword = intent.extras!!.getString(EXTRA_KEYWORD_ID)!!
        viewModel = ViewModelProvider(this, viewModelFactory).get(UsersListViewModel::class.java)
        viewModel.isSearching.observe(this, Observer {
            if(it){
                viewMvx.showProgressIndicator()
            } else {
                viewMvx.hideProgressIndication()
            }
        })

        lifecycleScope.launch {
            viewModel.searchUserFlow(
                SearchUserPagingSource(keyword, fetchResultOfUserSearchUseCase, dialogsNavigator))
                .collectLatest { viewMvx.suspendBindPagingData(it) }
        }
    }

    override fun onStart() {
        super.onStart()
        viewMvx.registerListener(this)
    }

    override fun onStop() {
        super.onStop()
        coroutineScope.coroutineContext.cancelChildren()
        viewMvx.unregisterListener(this)
    }

    override fun onBackClicked() {
        screensNavigator.navigateBack()
    }

    companion object{
        const val EXTRA_KEYWORD_ID = "EXTRA_KEYWORD_ID"
        fun start(context: Context, keyWord: String){
            val intent = Intent(context, UsersListActivity::class.java)
            intent.putExtra(EXTRA_KEYWORD_ID, keyWord)
            context.startActivity(intent)
        }
    }
}