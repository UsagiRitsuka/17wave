package wachi.waveproj.users

import android.util.Log
import androidx.paging.PagingSource
import wachi.waveproj.Constants
import wachi.waveproj.screens.common.dialogs.DialogsNavigator
import java.lang.Exception

class SearchUserPagingSource(
    private val keyword: String,
    private val fetchResultOfUserSearchUseCase: FetchResultOfUserSearchUseCase,
    private val dialogsNavigator: DialogsNavigator
): PagingSource<Int, User>() {

    interface Listener{
        fun onSearchStart()
        fun onSearchFinish()
    }

    var searchStateListener: Listener? = null

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, User> {
        try {
            val pageNo = params.key ?: 0
            var next: Int? = null
            var pre: Int? = null
            searchStateListener?.onSearchStart()
            val result = fetchResultOfUserSearchUseCase.fetchUserSearch(keyword, Constants.PER_COUNT, pageNo)
            val users: List<User> =
                when(result){
                    is FetchResultOfUserSearchUseCase.Result.Success -> {
                        pre = if (pageNo == 0) null else pageNo - 1
                        next = if(result.searchResult.totalPageNumber > (pageNo + 1) * Constants.PER_COUNT) pageNo + 1 else null
                        result.searchResult.users
                    }

                    is FetchResultOfUserSearchUseCase.Result.Failure -> {
                        dialogsNavigator.showServerErrorDialog()
                        ArrayList()
                    }
                }

            return LoadResult.Page(users, pre, next)
        } catch (e: Exception){
            return LoadResult.Error(e)
        } finally {
            searchStateListener?.onSearchFinish()
        }
    }
}