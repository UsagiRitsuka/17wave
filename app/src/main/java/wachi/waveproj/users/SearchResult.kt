package wachi.waveproj.users

data class SearchResult(
    val pageNo: Int,
    val totalPageNumber: Int,
    val users: List<User>
)

data class User(
    val avatarUrl: String,
    val name: String
)