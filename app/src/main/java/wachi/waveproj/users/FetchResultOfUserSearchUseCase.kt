package wachi.waveproj.users

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import wachi.waveproj.networking.GithubApi
import javax.inject.Inject

class FetchResultOfUserSearchUseCase @Inject constructor(private val githubApi: GithubApi) {
    sealed class Result {
        data class Success(val searchResult: SearchResult) : Result()
        object Failure: Result()
    }

    suspend fun fetchUserSearch(keyword: String, perPage: Int, pageNo: Int): Result{
        return withContext(Dispatchers.IO) {
            try {
                val response = githubApi.searchUser(keyword, perPage, pageNo)
                if (response.isSuccessful && response.body() != null) {
                    val searchResult = response.body()!!
                    val users = ArrayList<User>()
                    searchResult.items.forEach {
                        users.add(User(it.avatar_url, it.login))
                    }

                    return@withContext Result.Success(
                            SearchResult(pageNo, response.body()!!.total_count, users))
                } else {
                    return@withContext Result.Failure
                }
            } catch (t: Throwable) {
                if (t !is CancellationException) {
                    return@withContext Result.Failure
                } else {
                    throw t
                }
            }
        }
    }

}