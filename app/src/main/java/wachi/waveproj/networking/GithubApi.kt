package wachi.waveproj.networking

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import wachi.waveproj.Constants

interface GithubApi {
    @GET("/search/users")
    suspend fun searchUser(
        @Query("q") keyword: String,
        @Query("per_page") perPage: Int,
        @Query("page") pageNo: Int
    ): Response<SearchUserResult>
}